# Crypto tasks
### 1. DM Collision
###### Описание таска и исходники
>Can you find a collision in this compression [function](https://storage.googleapis.com/gctf-2018-attachments/155af75c13b85a3ebac4e324186062b6d2bc9c705957c26a07fe26bdb30cef8d)?

###### Своими словами
>Нужно передать в одностороннюю функцию Дэвиса-Майра на "самопальном" DES, такое значение, которое бы вызвало три определенных коллизии

###### Разборы
>[1](https://github.com/nguyenduyhieukma/CTF-Writeups/tree/master/Google%20CTF%20Quals/2018/Crypto-DM-COLLISION)
[2](https://mhackeroni.it/archive/2018/06/29/google-ctf-2018-dm-collision.html)
[3](https://fortenf.org/e/ctfs/crypto/2018/06/26/google-ctf-2018-dm-collision.html)

### 2. [L]ooking [F]or the [S]upe[R]man
###### Описание таска и исходники
>Victor has always been a bit shifted on the Soviet technologies, but when he left me some strange looking [scheme](http://school-ctf.org/files/picture_df5fd0a9cef3a7bbfa13bcb7170fedafd96f4453.jpg) and bunch of numbers instead of the password to the deployment server it crossed the line!
>Can you help me to restore the password from THAT?

```
Y = 000001000000000010001110111111111100000110101011100011100111010100011011111101001010100100110100100010000110110101101101101111010100011001111010001100000101010110000000010011001000010000100110100001111011010111000010000111101110001110001110
```

###### [Разбор](https://ctftime.org/writeup/7932)

